package hd44780

import (
	"bytes"
	"fmt"
	"image"
	"io"
	"log"
	"time"
	"unicode"

	"github.com/d2r2/go-i2c"
)

type LcdType int

const (
	LcdType_Unknown LcdType = iota
	LcdType_16x2
	LcdType_20x4
)

func (t LcdType) Bounds() image.Rectangle {
	origin := image.Pt(0, 0)
	maxPt := image.Pt(0, 0)
	switch t {
	case LcdType_16x2:
		maxPt = image.Pt(16, 2)
	case LcdType_20x4:
		maxPt = image.Pt(20, 4)
	}
	return image.Rectangle{origin, maxPt}
}

type Lcd struct {
	i2c           *i2c.I2C
	enabled       bool
	backlight     bool
	bounds        image.Rectangle
	cursorEnabled bool
	cursorBlink   bool
	cursorPos     image.Point
}

var rowAddrs = []byte{0x00, 0x40, 0x14, 0x54}

func NewLcd(i2c *i2c.I2C, backlight bool, lcdType LcdType) (*Lcd, error) {
	lcd := &Lcd{
		i2c:           i2c,
		enabled:       true,
		backlight:     true,
		bounds:        lcdType.Bounds(),
		cursorEnabled: false,
		cursorBlink:   false,
	}

	// See http://www.sprut.de/electronic/lcd/#init for initial sequence
	initBytes := []delayData{
		{0x03, 0, 5 * time.Millisecond},
		{0x03, 0, 100 * time.Microsecond},
		{0x03, 0, 0},
		{0x02, 0, 0}, // Set to 4-Bit mode
		{CMD_FUNCTION | OPT_2_Lines | OPT_5x8_Dots | OPT_4Bit_Mode, 0, 100 * time.Microsecond},
		{CMD_DISPLAY_CONTROL, 0, 0},
		{CMD_CLEAR_DISPLAY, 0, 0},
		{CMD_ENTRY_MODE | OPT_Increment, 0, 0},
		{CMD_DISPLAY_CONTROL | OPT_Enable_Display, 0, 0},
	}

	if _, err := lcd.writeBytesWithDelay(initBytes); err != nil {
		return nil, err
	}

	return lcd, nil
}

func (l *Lcd) SetCursor(row, col int) error {
	w, h := l.bounds.Dx(), l.bounds.Dy()

	if col < 0 || col >= w {
		return fmt.Errorf("Column argument %d must be in range [0..%d", col, w-1)
	}
	if row < 0 || row >= h {
		return fmt.Errorf("Row argument %d must be in range [0..%d]", row, h-1)
	}

	posByte := rowAddrs[row] + byte(col)
	if err := l.Command(CMD_DDRAM, posByte); err != nil {
		return fmt.Errorf("Error while sending SetCursor Command: %v", err)
	}
	l.cursorPos = image.Pt(col, row)
	return nil
}

// Increase cursor by one and handle line breaks.
func (l *Lcd) cursorInc() error {
	newCol := l.cursorPos.X + 1
	newRow := l.cursorPos.Y
	if newCol >= l.bounds.Dx() {
		newCol = 0
		newRow++
		if err := l.SetCursor(newRow, newCol); err != nil {
			return fmt.Errorf("CursorError: End of Display")
		}
	}
	l.cursorPos = image.Pt(newCol, newRow)
	return nil
}

func (l *Lcd) cursorDec() error {
	newCol := l.cursorPos.X - 1
	newRow := l.cursorPos.Y
	if newCol < 0 {
		newCol = l.bounds.Dx() - 1
		newRow--
		if err := l.SetCursor(newRow, newCol); err != nil {
			return fmt.Errorf("CursorError: Start of Display")
		}
	}
	l.cursorPos = image.Pt(newCol, newRow)
	return nil
}

func (l *Lcd) Write(buf []byte) (int, error) {
	var writeCount int

	reader := bytes.NewReader(buf)
	for {
		r, s, err := reader.ReadRune()
		l.WaitReady()
		if err == io.EOF {
			break
		} else if err != nil {
			return writeCount, fmt.Errorf("Error while reading buf: %v", err)
		}

		if unicode.IsControl(r) {
			switch r {
			case '\n':
				newLine := l.cursorPos.Y + 1
				if err := l.SetCursor(newLine, 0); err != nil {
					return writeCount, fmt.Errorf("Writing outside display bounds: %v", err)
				}
			case '\r':
				l.SetCursor(l.cursorPos.Y, 0)
			case '\b':
				l.cursorDec()
			}
			writeCount += s
			continue
		}

		encodedRune := Encode(r)
		for _, b := range encodedRune {
			if err := l.writeByte(b, PIN_RS.High()); err != nil {
				return writeCount, fmt.Errorf("Error while writing rune byte: %v", err)
			}
			writeCount++
			if err := l.cursorInc(); err != nil {
				return writeCount, fmt.Errorf("Error while increasing cursor: %v", err)
			}
		}
	}

	return writeCount, nil
}

// WriteString is a wrapper for Write(), casting`s` to a byte slice.
func (l *Lcd) WriteString(s string) (int, error) {
	return l.Write([]byte(s))
}

// writeWithStrobe lays the data onto the bus and pulls the enable pin to high.
// This tells the controller to read the data on the bus. After that enable must be
// set to low again. For more information see: http://www.sprut.de/electronic/lcd/#interface
func (l *Lcd) writeWithStrobe(b byte) error {
	var data byte

	if l.backlight {
		b |= PIN_BACKLIGHT.High()
	}

	// Write Data to data bus
	data = b
	if _, err := l.i2c.WriteBytes([]byte{data}); err != nil {
		return err
	}

	// Set the Enable pin to high. This makes the lcd controller read the data
	data = b | PIN_EN.High()
	if _, err := l.i2c.WriteBytes([]byte{data}); err != nil {
		return err
	}
	time.Sleep(500 * time.Microsecond)

	// Set Enable PIN back to low.
	data = b & (^PIN_EN.High())
	if _, err := l.i2c.WriteBytes([]byte{data}); err != nil {
		return err
	}
	time.Sleep(30 * time.Microsecond)

	return nil
}

func (l *Lcd) writeByte(data byte, ctrlPins byte) error {
	l.WaitReady()
	highNibble := (data & 0xF0)
	lowNibble := ((data << 4) & 0xF0)
	if err := l.writeWithStrobe(highNibble | ctrlPins); err != nil {
		return err
	}
	if err := l.writeWithStrobe(lowNibble | ctrlPins); err != nil {
		return err
	}

	return nil
}

type delayData struct {
	data, ctrlPins byte
	pause          time.Duration
}

func (l *Lcd) writeBytesWithDelay(data []delayData) (int, error) {
	for i, d := range data {
		if err := l.writeByte(d.data, d.ctrlPins); err != nil {
			return i, err
		}
		time.Sleep(d.pause)
	}
	return len(data), nil
}

// Command executes command `cmd`. Blocks until Display is not busy anymore.
// `cmd` must be a valid command. For valid commands see constants of this package.
func (l *Lcd) Command(cmd byte, settings byte) error {
	if err := l.writeByte(cmd|settings, 0); err != nil {
		return err
	}
	return nil
}

// Home executes the RETURN_HOME command.
// The cursor will be set to the initial position.
func (l *Lcd) Home() error {
	return l.Command(CMD_RETURN_HOME, 0)
}

// Clear clears the display and sets the cursor to the initial position.
func (l *Lcd) Clear() error {
	if err := l.Command(CMD_CLEAR_DISPLAY, 0); err != nil {
		return err
	}
	return l.Home()
}

func (l *Lcd) readWithStrobe(rs bool, buff []byte) (int, error) {
	var readCount int

	data := PIN_RW.High()
	if rs {
		data |= PIN_RS.High()
	}
	if l.backlight {
		data |= PIN_BACKLIGHT.High()
	}

	if _, err := l.i2c.WriteBytes([]byte{data}); err != nil {
		return readCount, err
	}

	// Set Enable to High
	if _, err := l.i2c.WriteBytes([]byte{data | PIN_EN.High()}); err != nil {
		return readCount, err
	}
	time.Sleep(500 * time.Microsecond) // Wait for controller

	// Read data to buffer
	readCount, err := l.i2c.ReadBytes(buff)
	if err != nil {
		return readCount, err
	}

	if _, err := l.i2c.WriteBytes([]byte{data & PIN_EN.Low()}); err != nil {
		return readCount, err
	}
	time.Sleep(30 * time.Microsecond)

	return readCount, nil
}

func (l *Lcd) readByte(register bool) (byte, error) {
	var res byte
	buff := make([]byte, 1)

	// Read upper 4Bits
	if _, err := l.readWithStrobe(register, buff); err != nil {
		return res, fmt.Errorf("Error reading high Nibble: %v", err)
	}
	res |= buff[0] & 0xF0

	// Read lower 4 Bits
	if _, err := l.readWithStrobe(register, buff); err != nil {
		return res, fmt.Errorf("Error reading low Nibble: %v", err)
	}
	res |= ((buff[0] & 0xF0) >> 4)

	return res, nil
}

// Busy returns true if the display is busy.
// While the display is busy, no new commands must be send.
func (l *Lcd) Busy() bool {
	b, err := l.readByte(false)
	if err != nil {
		log.Printf("Error in Busy(): %v", err)
		log.Printf("Read byte was %d", b)
	}
	return ((b >> 7) & 0x01) == 1
}

// WaitReady blocks until the LCD is ready to accept new commands.
func (l *Lcd) WaitReady() {
	for l.Busy() {
		time.Sleep(50 * time.Microsecond)
	}
}

// SignalReady signals if the Display is ready again in a non-blocking way.
// A channel is returnd which will hold `true` is the display is ready again.
func (l *Lcd) SignalReady() chan bool {
	readyChan := make(chan bool)
	go func() {
		for l.Busy() {
			time.Sleep(50 * time.Microsecond)
		}
		readyChan <- true
		close(readyChan)
	}()
	return readyChan
}
