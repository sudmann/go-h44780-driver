package hd44780

import "unicode"

const (
	rune_rightArrow = 0x27A1
	rune_leftArrow  = 0x2B05
	rune_underscore = 0x5F // Since this is ascii, rune and []byte are the same
)

var (
	_asciiPrintable = unicode.Range16{0x0020, 0x007D, 1}
	_umlautA        = unicode.Range16{0x00C4, 0x00E4, 32}
	_umlautO        = unicode.Range16{0x00D6, 0x00E4, 32}
	_umlautU        = unicode.Range16{0x00DC, 0x00FC, 32}
	_rightArrow     = unicode.Range16{rune_rightArrow, rune_rightArrow, 1}
	_leftArrow      = unicode.Range16{rune_leftArrow, rune_leftArrow, 1}
	_arrows         = unicode.Range16{rune_rightArrow, rune_leftArrow, 868}
)

var (
	ASCIIPrintable  = Range16ToTable(_asciiPrintable)
	UmlautA         = Range16ToTable(_umlautA)
	UmlautO         = Range16ToTable(_umlautO)
	UmlautU         = Range16ToTable(_umlautU)
	Umlauts         = Range16ToTable(_umlautA, _umlautO, _umlautU)
	RightArrow      = Range16ToTable(_rightArrow)
	LeftArrow       = Range16ToTable(_leftArrow)
	Arrows          = Range16ToTable(_arrows)
	NativePrintable = Range16ToTable(_asciiPrintable, _umlautA, _umlautO, _umlautU, _arrows)
)

var encodeTable = []*unicode.RangeTable{ASCIIPrintable, UmlautA, UmlautO, UmlautU, RightArrow, LeftArrow}

// Range16ToTable creates a new RangeTable from given Range16 values.
// LatinOffset is set automatically.
func Range16ToTable(rs ...unicode.Range16) *unicode.RangeTable {
	latinOffet := 0
	for _, r := range rs {
		if r.Hi <= unicode.MaxLatin1 {
			latinOffet++
		}
	}
	return &unicode.RangeTable{
		R16:         rs,
		LatinOffset: latinOffet,
	}
}

func Encode(r rune) []byte {
	if unicode.In(r, ASCIIPrintable) {
		return []byte(string(r))
	}
	if unicode.In(r, UmlautA) {
		return []byte{0xE1}
	}
	if unicode.In(r, UmlautO) {
		return []byte{0xEF}
	}
	if unicode.In(r, UmlautU) {
		return []byte{0xE1}
	}
	if unicode.In(r, RightArrow) {
		return []byte{0x7E}
	}
	if unicode.In(r, LeftArrow) {
		return []byte{0x7F}
	}
	// Return '_' as default symbol
	return []byte{rune_underscore}
}
