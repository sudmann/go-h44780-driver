package hd44780

// Commands
const (
	CMD_CLEAR_DISPLAY = (1 << iota)
	CMD_RETURN_HOME
	CMD_ENTRY_MODE
	CMD_DISPLAY_CONTROL
	CMD_CURSOR_DISPLAY_SHIFT
	CMD_FUNCTION
	CMD_CGRAM
	CMD_DDRAM
)

// Options
const (
	// CMD_ENTRY_MODE
	OPT_Increment = 0x02
	OPT_Decrement = 0x00
	// OPT_Display_Shift  = 0x01 // CMD_Entry_Mode
	OPT_Enable_Display = 0x04 // CMD_Display_Control
	OPT_Enable_Cursor  = 0x02 // CMD_Display_Control
	OPT_Enable_Blink   = 0x01 // CMD_Display_Control
	OPT_Display_Shift  = 0x08 // CMD_Cursor_Display_Shift
	OPT_Shift_Right    = 0x04 // CMD_Cursor_Display_Shift 0 = Left
	OPT_8Bit_Mode      = 0x10
	OPT_4Bit_Mode      = 0x00
	OPT_2_Lines        = 0x08 // CMD_Function_Set 0 = 1 line
	OPT_1_Lines        = 0x00
	OPT_5x10_Dots      = 0x04 // CMD_Function_Set 0 = 5x7 dots
	OPT_5x8_Dots       = 0x00
)

type Pin byte

const (
	PIN_RS Pin = (1 << iota)
	PIN_RW
	PIN_EN
	PIN_BACKLIGHT
)

// High returns the corresponding byte if the pin set to high (Pin = 1)
func (p Pin) High() byte {
	return byte(p)
}

// Low returns the bitwise complement of the pin's high value.
// This makes it safe to use for &-Operations
func (p Pin) Low() byte {
	return byte(^p)
}
